# This file is part of Tryton. The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
import copy


class Address(ModelSQL, ModelView):
    _name = 'party.address'

    full_name = fields.Function(fields.Char('Full Name'), 'get_full_name')
    address_without_name = fields.Function(fields.Char('Address w/o Name'),
            'get_address_without_name')
    name2 = fields.Char('Add. Addr. Inf.')

    def __init__(self):
        super(Address, self).__init__()
        self.name = copy.copy(self.name)
        self.name.string = 'Alternative Addressee'
        self._reset_columns()

    def get_full_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        for address in self.browse(ids):
            if address.name:
                res[address.id] = address.name
            else:
                res[address.id] = address.party.full_name
        return res

    def get_address_without_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        for address in self.browse(ids):
            res[address.id] = ''
            if address.name2:
                res[address.id] += address.name2 + '\n'
            if address.street:
                res[address.id] += address.street +'\n'
            if address.streetbis:
                res[address.id] += address.streetbis + '\n'
            if address.zip or address.city:
                if address.zip:
                    res[address.id] += address.zip + ' '
                if address.city:
                    res[address.id] += address.city
                res[address.id] += '\n'
            if address.subdivision:
                res[address.id] += address.subdivision.name + '\n'
            if address.country:
                res[address.id] += address.country.name
        return res

    def get_full_address(self, ids, name):
        if not ids:
            return {}
        res = {}
        for address in self.browse(ids):
            res[address.id] = ''
            if address.full_name:
                res[address.id] += address.full_name
            if address.address_without_name:
                if res[address.id]:
                    res[address.id] += '\n'
                res[address.id] += address.address_without_name
        return res

    def get_rec_name(self, ids, name):
        if not ids:
            return {}
        res = {}
        for address in self.browse(ids):
            res[address.id] = ", ".join(x for x in [
                    address.full_name, address.zip, address.city,
            ] if x)
        return res

Address()
