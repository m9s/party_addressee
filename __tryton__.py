# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Party Addresse',
    'name_de_DE': 'Partei Adressat',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
        - Provides detailed use and label for field 'name' on addresses
        - Adds field for input of additional address data
    ''',
    'description_de_DE': '''
        - Ermöglicht detaillierte Benennung und Gebrauch von Feld 'Name' für
          Adressen
        - Fügt Feld 'Zustellzusatz' hinzu
    ''',
    'depends': [
        'party'
    ],
    'xml': [
        'party.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
